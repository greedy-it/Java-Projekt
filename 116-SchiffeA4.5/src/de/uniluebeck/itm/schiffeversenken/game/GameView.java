package de.uniluebeck.itm.schiffeversenken.game;

import de.uniluebeck.itm.schiffeversenken.engine.Application;
import de.uniluebeck.itm.schiffeversenken.engine.AssetRegistry;
import de.uniluebeck.itm.schiffeversenken.engine.Canvas;
import de.uniluebeck.itm.schiffeversenken.engine.Tile;
import de.uniluebeck.itm.schiffeversenken.engine.Vec2;
import de.uniluebeck.itm.schiffeversenken.engine.View;
import de.uniluebeck.itm.schiffeversenken.game.model.GameModel;

/**
 * GameView shows the playground with hidden ships, the score and who is ready
 * for bombing
 * 
 * modified by C.Horn and J.Schwarz
 * 
 */

public class GameView extends View<GameModel> {

	private final GameFieldRenderer fieldRenderer;
	private final GameFieldRenderer opponentFieldRenderer;

	public GameView(GameModel m) {
		super(m);
		this.fieldRenderer = new GameFieldRenderer(this.getModelInstance().getHumanPlayerField());
		// this.opponentFieldRenderer = new
		// GameFieldRenderer(this.getModelInstance().getComputerPlayerField());

		// hide the opponent ships w/ new class HitMissRenderer
		this.opponentFieldRenderer = new HitMissRenderer(this.getModelInstance().getComputerPlayerField());
	}

	@Override
	public void render(Canvas c, Vec2 mouseLocation) {
		final int frameWidth = c.getResolutionWidth(), frameHeight = c.getResolutionHeight();
		final int offsetX = 10, offsetY = 35;

		final GameModel model = this.getModelInstance();
		final Vec2 gameFieldDimensions = model.getHumanPlayerField().getSize();

		final int fieldsWidth = gameFieldDimensions.getX() * Constants.TILE_SIZE;
		final int fieldsHeight = gameFieldDimensions.getX() * Constants.TILE_SIZE;
		final int opponentsFieldX = offsetX + fieldsWidth + 10;
		this.fieldRenderer.renderGameField(c, offsetX, offsetY);
		this.opponentFieldRenderer.renderGameField(c, opponentsFieldX, offsetY);
		model.updateOpponentsFieldOnScreenData(new Vec2(opponentsFieldX, offsetY), new Vec2(fieldsWidth, fieldsHeight));

		// The tile (box) on which the mouse pointer is positioned is highlighted in
		// color as
		// the highlighted as a target aid.
		this.opponentFieldRenderer.renderMouseOver(c, mouseLocation.getX(), mouseLocation.getY(), opponentsFieldX, offsetY);

		c.setColor(0.7, 0.7, 0.7);
		c.drawRoundRect(frameWidth - 280 - offsetX, offsetY, 280, frameHeight - offsetY - 100, 5, 5);

		final int[] numbers = new int[] { model.getRoundCounter(), model.getPlayerPoints(), model.getAiPoints() };
		final String[] labels = new String[] { "Round: ", "Your points", "Computers points" };
		for (int i = 0; i < numbers.length; i++) {
			// Irgendwie muss man da noch dinge mit dem Offset machen...
			draw7segNumberAt(c, frameWidth - offsetX - 45, offsetY + 25 + (i * 50), numbers[i]);
			c.drawString(frameWidth - 270 - offsetX, offsetY + 35 + (i * 50), labels[i]);
		}

		if (model.isRoundChanging()) {
			c.setColor(0.7, 0.7, 0.7, 0.7);
			c.fillRect(0, 0, frameWidth, frameHeight);
			c.setColor(0, 0, 0);
			final String text = "Round changing. Please wait for the AI to destroy you.";
			final Vec2 textDim = c.getTextDimensions(text);
			c.drawString(frameWidth / 2 - textDim.getX() / 2, frameHeight / 2 - textDim.getY() / 2, text);
		}
	}

	/**
     * A real (well sort of, it's still inside a computer) 7 segment display
     * @param c Canvas
     * @param x x-coordinate for 7Seg display
     * @param y y-coordinate for 7Seg display
     * @param number score/round number
     */
    private void draw7segNumberAt(Canvas c, int x, int y, int number) {
    	// Application.log("number " + number);
        // c.drawString(x, y, Integer.toString(number));
        // TODO implement a real (well sort of, it's still inside a computer) 7 segment display
    	
    	// color for 7Seg display scoreboard
    	c.setColor(1.0, 1.0, 1.0);
    	// 4 if's show backgrounds for the 7Seg display from 1 digit to 4 digit number
    	if(number <=9) {
    		c.fillRect(x-5, y-2, 28, 44);
    	}
    	if(number >= 10 && number <= 99) {
    		c.fillRect(x-5, y-2, 28, 44);
    		c.fillRect(x-30, y-2, 28, 44);
    	}
    	if(number >= 100 && number <= 999) {
    		c.fillRect(x-5, y-2, 28, 44);
    		c.fillRect(x-30, y-2, 28, 44);
    		c.fillRect(x-55, y-2, 28, 44);
    	}
    	if(number >= 1000 && number <= 9999) {
    		c.fillRect(x-5, y-2, 28, 44);
    		c.fillRect(x-30, y-2, 28, 44);
    		c.fillRect(x-55, y-2, 28, 44);
    		c.fillRect(x-80, y-2, 28, 44);
    	}
    	
    	// to get the lonely digits
    	int oneDigit   = number % 10;
        int twoDigit   = (number / 10) % 10;
        int threeDigit = (number / 100) % 10;
        int fourDigit  = (number / 1000) % 10;
        
        // get and draw the correct 7Seg Number
        for(int i = 0; i <= 9; i++) {
        	// check if i is the correct number
        	if(i == oneDigit) {
        		// sevebSegNumber gets the correct 7Seg Image
    			Tile sevenSegNumber = AssetRegistry.getTile("7seg."+i);
    			// the 7Seg Image will be placed on the correct position
    			Vec2 sevenSegPosition = new Vec2(x,y);
    			// if you comment the 4 if's above and uncomment these next c.fillRect's
    			// all 4 backgrounds will show up
    			// c.fillRect(x-5, y-2, 28, 44);
    			// draw the correct 7Seg Number on the right place
    			sevenSegNumber.renderAt(c, sevenSegPosition);
    		}
        	if(i == twoDigit) {
    			Tile sevenSegNumber = AssetRegistry.getTile("7seg."+i);
    			Vec2 sevenSegPosition = new Vec2(x-25,y);
    			// c.fillRect(x-30, y-2, 28, 44);
    			sevenSegNumber.renderAt(c, sevenSegPosition);
    		}
        	if(i == threeDigit) {
    			Tile sevenSegNumber = AssetRegistry.getTile("7seg."+i);
    			Vec2 sevenSegPosition = new Vec2(x-50,y);
    			// c.fillRect(x-55, y-2, 28, 44);
    			sevenSegNumber.renderAt(c, sevenSegPosition);
    		}
        	if(i == fourDigit) {
    			Tile sevenSegNumber = AssetRegistry.getTile("7seg."+i);
    			Vec2 sevenSegPosition = new Vec2(x-75,y);
    			// c.fillRect(x-80, y-2, 28, 44);
    			sevenSegNumber.renderAt(c, sevenSegPosition);
    		}
    	}
    }

	@Override
	public void prepare() {

	}
}
