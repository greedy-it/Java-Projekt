package de.uniluebeck.itm.schiffeversenken.main;

/**
 * Vorbereitende Uebung zum 2D-Array
 * Eine Klasse zur Erstellung eines 2D-Arrays mit verschiedenen Modifikationen
 * @author C. Horn J. Schwarz, Gruppe 116
 * 
 */

public class MatrixTest {

	public static void main(String[] args) {

		// number of rows of 2D Matrix
		int row = 10;
		// number of cols of 2D Matrix
		int col = 10;
		// array size w/ numbers of rows and cols
		int[][] field = new int[row][col];
		// number to set for diagonalMatrix
		int x = 1;
		
		// execute the methods
		MatrixTest matrixTest = new MatrixTest();
		
		matrixTest.showMatrix(field);
		System.out.println();
		matrixTest.diagonalMatrix(x, field);
		System.out.println();
		matrixTest.columnSum(row, col, field);
		System.out.println();
		matrixTest.zeroFirstRow(field);
	}

	/**
	 * Initialize the Matrix
	 * @param row number of rows
	 * @param col number of columns
	 * @param field the 2D array w/ rows and numbers
	 */
	public void initMatrix(int row, int col, int[][]field) {
		int wert = 0;
		for(int j = 0; j < field.length; j++) {
			for(int i = 0; i < field.length; i++) {
				field[j][i] = wert++;
			}
		}
	}

	/**
	 * Transpose the Matrix, change rows and columns
	 * @param field the 2D array w/ rows and numbers
	 */
	public void transMatrix(int[][] field) {
		int tmp;
		for(int i=0; i < field.length; i++) {
			for(int j=i; j < field.length; j++) {	
				tmp = field[i][j];
				field[i][j] = field[j][i];
				field[j][i] = tmp;
			}
		}
	}  

	/**
	 * Prints the Matrix
	 * @param field the 2D array w/ rows and numbers
	 */
	public void showMatrix(int[][] field) {
		initMatrix(10,10,field);
		transMatrix(field);

		for(int i=0 ; i < field.length ; i++) {
			for(int j=0 ; j < field[0].length ; j++) {
				if(j<1) {
					System.out.print(0);
				}
				System.out.print(field[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Change the diagonal values of the Matrix
	 * @param x number to set for the diagonal values
	 * @param field the 2D array w/ rows and numbers
	 */
	public void diagonalMatrix(int x, int[][] field) {
		initMatrix(10,10,field);
		transMatrix(field);

		for(int i=0 ; i < field.length ; i++) {
			for(int j=0 ; j < field[0].length ; j++) {
				if(j==i) {
					field[i][j] = x;
				}
				if(j==0 && x < 10 ) {
					System.out.print(0);
				}
				else if(i==j && x < 10) {
					System.out.print(0);
				}
				else if(x > 9 && j==0 && i>0) {
					System.out.print(0);
				}
				System.out.print(field[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Gives the sum of each column
	 * @param row number of rows
	 * @param col number of columns
	 * @param field the 2D array w/ rows and numbers
	 */
	public void columnSum(int row, int col,int [][]field){
		initMatrix(10,10,field);
		transMatrix(field);

		int index = 0;
		for (int i = 0; i < field.length; i++) {
			int sum = 0;
			for (int j = 0; j < field.length; j++) {
				field[i][j] = i*10 + j;
				sum = sum + field[i][j];
			}
			index++;
			System.out.println("Sum of " + index + ". column is: "+sum);

		}
	}

	/**
	 * Set the first row to 0
	 * @param field the 2D array w/ rows and numbers
	 */
	public void zeroFirstRow(int[][] field) {
		initMatrix(10,10,field);
		transMatrix(field);

		for(int i=0 ; i < field.length ; i++) {
			for(int j=0 ; j < field[0].length ; j++) {
				field[0][j] = 0;
				if(j<1) {
					System.out.print(0);
				}
				else if(i==0) {
					System.out.print(0);
				}
				System.out.print(field[i][j] + " ");
			}
			System.out.println();
		}
	}
}