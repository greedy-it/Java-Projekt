package de.uniluebeck.itm.schiffeversenken.game.ai;

import java.util.Random;

import de.uniluebeck.itm.schiffeversenken.game.model.FieldTile;
import de.uniluebeck.itm.schiffeversenken.game.model.GameField;
import de.uniluebeck.itm.schiffeversenken.game.model.Ruleset;

/**
 * Second level of difficulty - as a tribute to the countless hours we have spent running and testing this program.
 * @author C. Horn
 *
 */
public class BetterAIAgent extends AIAgent {
	
	private FieldTile lastTile;
	//shooting coordinates
	int shootX, shootY;
	
	public BetterAIAgent(int hardness) {
        super(hardness);
    }

	@Override
    public void setup(Ruleset r, GameField agentsField) {
        this.placeShipsAccordingToRules(r, agentsField);
    }

    @Override
    public boolean performMove(GameField playersField) {
        // final Random rnd = new Random(System.currentTimeMillis());
        // final int fieldWidth = playersField.getSize().getX();
        // final int fieldHeight = playersField.getSize().getY();
        FieldTile tile;
        //x and y are placeholder for shootX/Y 
        int x = shootX;
        int y = shootY;
        do {
        	tile = playersField.getTileAt(x, y);
        } while (tile.wasAlreadyBombarded());
        //count the shootX
        x++;
        if(shootX >= 15 && shootY <= 15) {
        	//count shootY
        	y++;
        	//set shootX at zero
        	x=0;
        }
        setShooting(x, y);
        this.lastTile = tile;
        return tile.bombard();
    }
    /**
     * Use this method to save the last x/y-coordinates
     * @param x x-coordinate
     * @param y y-coordinate
     */
    public void setShooting(int x, int y) {
    	// save placeholder x to shootX/Y
    	shootX = x;
    	shootY = y;
    }

    @Override
    public FieldTile getLastAttackedTile() {
        return this.lastTile;
    }
}
