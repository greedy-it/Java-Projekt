package de.uniluebeck.itm.schiffeversenken.game;

import de.uniluebeck.itm.schiffeversenken.engine.Application;
import de.uniluebeck.itm.schiffeversenken.engine.Controller;
import de.uniluebeck.itm.schiffeversenken.engine.Vec2;
import de.uniluebeck.itm.schiffeversenken.game.ai.AIAgent;
import de.uniluebeck.itm.schiffeversenken.game.menues.EndOfGameMenu;
import de.uniluebeck.itm.schiffeversenken.game.model.FieldTile;
import de.uniluebeck.itm.schiffeversenken.game.model.GameField;
import de.uniluebeck.itm.schiffeversenken.game.model.GameModel;
import de.uniluebeck.itm.schiffeversenken.game.model.Ship;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Controller dienen dazu, auf Aktionen in der grafischen Oberfläche zu reagieren. 
 * Die Klasse GameController behandelt die Reaktion auf einen Mausklick in das gegnerische Spielfeld.
 * 
 * modified by C.Horn and J.Schwarz
 *
 */

public class GameController extends Controller<GameModel> {

    public GameController(GameModel m) {
        super(m);
    }

    @Override
    public void clickedAt(Vec2 mousePosition) {
        final GameModel model = this.getModelInstance();
        final Vec2 positionOnOpponentsField = mousePosition.add(model.getOpponentsFieldPosition().multiply(-1));
        final Vec2 opponentsFieldDimensions = model.getOpponentsFieldDimensions();

        // First of all we need to test if the player clicked within the field
        if(positionOnOpponentsField.getX() >= 0 && positionOnOpponentsField.getY() >= 0
                && positionOnOpponentsField.getX() < opponentsFieldDimensions.getX()
                && positionOnOpponentsField.getY() < opponentsFieldDimensions.getY()) {
            final int res = Constants.TILE_SIZE;
            
            // TODO implement a way to get the field coordinates
            // positionOnOpponentsField.getX() / res gives us the x coordinates on the 16x16 Matrix
            final int tileX = positionOnOpponentsField.getX() / res;
            // positionOnOpponentsField.getY() / res gives us the y coordinates on the 16x16 Matrix
            final int tileY = positionOnOpponentsField.getY() / res;
            
            Application.log("Bombarding position " + tileX + ", " + tileY);
            Application.log("OppFieldPos " + positionOnOpponentsField.getX() + ", " + positionOnOpponentsField.getY());
            

            final FieldTile fieldTile = model.getComputerPlayerField().getTileAt(tileX, tileY);
            if (!fieldTile.wasAlreadyBombarded()) {
                if(fieldTile.bombard()) {
                	
                    // TODO: the player hit something: do something with this information
                	// adds points for hitting a ship tile
                	model.addPlayerPoints(Constants.POINTS_FOR_HIT);
                	// adds points for destroying a ship 
                	// shows the hits of the ship
                	final Ship s = fieldTile.getCorrespondingShip();
                	// checks if the ship has already enough hits to be sunken
                	if(s != null && s.isSunken()) {
                		// adds points for sunken ship
                		model.addPlayerPoints(Constants.POINTS_FOR_SHIP_SUNK);
                	}
                	// method to show if all ships are sunken
                    handlePossibleGameEnd();
                	
                } else {
                    model.setRoundChangingFlag(true);
                    this.dispatchWork(new Runnable() {

                        @Override
                        public void run() {
                            while (model.getAgent().performMove(model.getHumanPlayerField())) {
                                rewardAgentForDestroyingPlayer();
                            }
                        }

                        private void rewardAgentForDestroyingPlayer() {
                            model.addAiPoints(Constants.POINTS_FOR_HIT);
                            final Ship s = model.getAgent().getLastAttackedTile().getCorrespondingShip();
                            if(s != null && s.isSunken()) {
                            	
                                // TODO perform the same checks for the computer player.
                            	model.addAiPoints(Constants.POINTS_FOR_SHIP_SUNK);
                            }
                            // method to show if all ships are sunken
                            handlePossibleGameEnd();
                        }
                    });
                    this.startWorkStack();
                }
            }
        }
    }
    
    /**
     * Checks with hilfsMethode(Ship[] shipsArr) if all ships are sunken
     * and open the endGame(boolean playerWon) function to present the winner
     */
    private void handlePossibleGameEnd() {

		// TODO check if the game ended
    	// load computer ships list as array
    	final Ship[] computerShips = this.getModelInstance().getComputerPlayerField().getCopyOfShipListAsArray();
    	// load human ships list as array
    	final Ship[] humanShips = this.getModelInstance().getHumanPlayerField().getCopyOfShipListAsArray();
    	
    	// check with hilfsMethode if all computer ships are sunken
    	if(checkAllShipsSunken(computerShips) == true) {
    		// if all comp ships are sunken, player is winner
    		endGame(true);
    	}
    	// check with hilfsMethode if all human ships are sunken
    	if(checkAllShipsSunken(humanShips) == true) {
    		// if all human ships are sunken, comp is winner
    		endGame(false);
    	}
    		
    }
    
    /**
     * Helping method to check if all ships are sunken
     * 
     * @param shipsArr 
     * @return true, gives a winner, false, the play goes on
     */
    private boolean checkAllShipsSunken(Ship[] shipsArr) {
    	// check variable for isSunken() in class Ship
    	Ship ships;
    	// counter
    	int count = 0;
    	// for-loop checks if all ships are sunken
    	for(int i = 0; i < shipsArr.length; i++) {
    		ships = shipsArr[i];
    		// if ship is sunken, count +1
    		if(ships.isSunken() == true) {
    			count++;
    		}
    	}
    	// all ships are sunken, return true, we have a winner!
    	if(count == shipsArr.length)
			return true;
		else
			// play goes on
			return false;
    }

    /**
     * Use this function in order to end the game and display the EndOfGameMenu.
     * @param playerWon Pass true if the player won and false if the computer won.
     */
    private void endGame(boolean playerWon) {
        Application.log("Ending game.");
        Application.switchToScene(new EndOfGameMenu(this.getModelInstance(), playerWon).getScene());
    }

    @Override
    public void keyPressed(int key, boolean shift, boolean alt, boolean ctrl, boolean down, boolean up, boolean left, boolean right) {
        // Doesn't do anything anymore since we've dropped multiple resolutions
    }

    @Override
    public void performFrequentUpdates() {
        if (!this.hasWork() && this.getModelInstance().isRoundChanging()) {
            // We need to assume that the round changing work has finished.
            this.getModelInstance().increaseRoundCounter();
            this.getModelInstance().setRoundChangingFlag(false);
        }
    }

    @Override
    public void prepare() {

    }
}
