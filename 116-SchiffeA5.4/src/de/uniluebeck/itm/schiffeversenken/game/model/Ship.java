package de.uniluebeck.itm.schiffeversenken.game.model;

/**
 * This class represents a ship.
 * @author leondietrich, modified by I. Schumacher, modified by C. Horn and J. Schwarz
 * 
 */
public class Ship {
	
	/**
	 * ship length
	 */
	private int length;
	
	/**
	 * counter for ship hits
	 */
	private int hits;
	
	/**
	 * orientation of the ship (vertical or horizontal)
	 */
	private boolean orientation;

	/**
	 * This constructor constructs a new ship.
	 * @param length The length of the ship to create.
	 * @param orientation The orientation of the ship: true for vertical, false for horizontal
	 * 
	 * modified by C. Horn and J. Schwarz
	 * 
	 */
	public Ship(int length, boolean orientation) {
		// length of the ship
		this.length = length;
		// counter for hits
		hits = 0;
		// orientation vertical or horizontal
		this.orientation = orientation;
	}

	/**
	 * Checks if a ship is sunken or not
	 * @return true or false if ship is sunken or not
	 */
	public boolean isSunken() {
		return this.hits == this.length;
	}
	
	/**
	 * Count the hits +1 when ship is not yet sunken
	 */
	public void hit() {
		if (!isSunken()) {
			hits++;
		}		
	}

	/**
	 * get()-Method for the orientation of the ship
	 * @return orientation of the ship (true = vertical, false = horizontal)
	 */
	public boolean isUp() {
		return this.orientation;
	}

}
