package de.uniluebeck.itm.schiffeversenken.game.model;

import java.util.*;
import java.util.function.Consumer;

import de.uniluebeck.itm.schiffeversenken.engine.Vec2;
import de.uniluebeck.itm.schiffeversenken.game.model.FieldTile.FieldTileState;

/**
 * This class represents a players field. There are at least two instances of
 * this class in a game.
 * 
 * @author leondietrich, modified by I. Schumacher, modified by C. Horn and J. Schwarz
 *         
 */
public final class GameField {

	/**
	 * The number of columns (x) and rows (y) for creating the 2D FieldTile array.
	 */
	private final Vec2 size;
	
	/**
	 * Variable for the 2D-FieldTile-Arrays
	 */
	private final FieldTile[][] field;
	
	/**
	 * To manage the ships
	 */
	private List<Ship> ships;

	/**
	 * Construct a new game field.
	 * 
	 * @param size The size of the new game field to use.
	 * 
	 * modified by C. Horn and J. Schwarz
	 * 
	 */
	public GameField(Vec2 size) {
		// Initialize the instance variable size
		this.size = size;
		// FieldTile-Array [x cols][y rows]
		this.field = new FieldTile[size.getX()][size.getY()];
		// double for-loop creates the gamefield
		// outter for-loop iterates over cols
		for (int x = 0; x < size.getX(); x++) {
			// inner for-loop iterates over rows
			for (int y = 0; y < size.getY(); y++) {
				// initialize each box of the 2D array with a new FieldTile object.
				field[x][y] = new FieldTile();
			}
		}
		// initialize new, empty LinkedList
		this.ships = new LinkedList<>();
	}

	/**
	 * Use this method in order to get the fields size.
	 * 
	 * @return The size of the field.
	 */
	public Vec2 getSize() {
		return this.size;
	}

	/**
	 * Use this method in order to get the tile at the desired location.
	 * 
	 * @param x The x coordinate
	 * @param y The y coordinate
	 * @return The located tile
	 */
	public FieldTile getTileAt(int x, int y) {
		if (this.size.getX() < x || this.size.getY() < y)
			throw new RuntimeException("Field tile out of bounds");
		return this.field[x][y];
	}

	/**
	 * Use this method in order to place ships on the game field.
	 * 
	 * @param posX        The x coordinate where the ship should begin
	 * @param posY        The y coordinate where the ship should begin
	 * @param length      The length of the ship to place.
	 * @param up          True if the ship should be placed vertically; false
	 *                    otherwise
	 * @param shipToPlace The ship instance to place
	 * 
	 * modified by C. Horn and J. Schwarz
	 * 
	 */
	public void placeShip(int posX, int posY, int length, boolean up, Ship shipToPlace) {
		/*
		 * Nach korrekter Implementation sollten sich die Schiffe korrekt vertikal und
		 * horizontal mit der richtigen Laenge platzieren lassen. Sie werden aber noch
		 * aus falschen Tiles zusammengesetzt.
		 */
		
		// with the help of getTileAt() method from the GameField class to get the tiles belonging to the ship (tiles)
		for (int currentShipsX = posX, currentShipsY = posY, i = 0; i < length; i++) {
			// consider the orientation
			if (up) {
				// use getTileAt() from the GameField to get tiles belonging to the ship (tiles)
				getTileAt(currentShipsX, currentShipsY).setCorrespondingShip(shipToPlace);
				// change the state of the tile
				getTileAt(currentShipsX, currentShipsY).setTilestate(FieldTile.FieldTileState.STATE_SHIP);
				// go to the next tile
				currentShipsY++;
			} else {
				// use getTileAt() from the GameField to get tiles belonging to the ship (tiles)
				getTileAt(currentShipsX, currentShipsY).setCorrespondingShip(shipToPlace);
				// change the state of the tile
				getTileAt(currentShipsX, currentShipsY).setTilestate(FieldTile.FieldTileState.STATE_SHIP);
				// go to the next tile
				currentShipsX++;
			}
		}
		// adds ship to the list
		this.ships.add(shipToPlace);
	}

	/**
	 * This method passes the lambda action to the java implementation of a
	 * distributed for each action.
	 * 
	 * @param action The action to perform while iterating
	 */
	public void iterateOverShips(Consumer<Ship> action) {
		this.ships.forEach(action);
	}

	/**
	 * An array containing a momentary copy of the ship list.
	 * 
	 * @return An Array of the current ships.
	 */
	public Ship[] getCopyOfShipListAsArray() {
		Ship[] arr = new Ship[this.ships.size()];
		return this.ships.toArray(arr);
	}

}
