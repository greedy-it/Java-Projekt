                                   Fragen zum Aufgabenblatt 04

1. Skizze anfertigen zum Lösen der Methode renderMouseOver() mit den verschiedenen Koordinatenangaben, die benötigt werden.

2. Wie berechnet man Spalte/Zeile des Kästchens, das überdeckt werden soll?
	- um die Spalte/Zeile des Kästchens zu berechenen muss man die Variablen fieldX und fieldY durch
	  die länge eines Tiles teilen und man bekommt Koordinaten von 0 bis 15 für x und y.
	  
3. Wie erhält man daraus die Koordinaten für das transparente Feld (Kästchen)?
	- man erhält die Koordinaten indem man tileX und tileY wieder mal die Konstante Tile size rechnet.
	  nach der Berechnung am besten eine Kontrollausgabe auf die Konsole ergänzen.
	  
4. Welche Parameter benötigen c.setColor() und c.fillRect() ?
	- c.setColor() benötig vier Parameter um eine farbe zu haben.
	- c.fillRect() benötig erst 2 Parameter welche die Koordinaten des feldes sind und
	  dann 2 Parameter für die Größe des Feldes.
	  
5. Was müssen Sie berücksichtigen beim Zeichnen des Pfeiles?
	- die richtige Platzierung des Pfeils

6. Wie ermitteln Sie die notwendigen Parameter für den Aufruf von renderMouseOver() in der Klasse GameView?
	- man ermittelt die Parameter indem geschaut wird was gebraucht wird damit renderMouseOver() funktioniert, 
	  hierfür wird dann die variable c welche für die Canvas benötigt wird übergeben, sowie die x und y Koordinaten der Maus. Zuletzt werden noch die Koordinaten des Feldes des Gegners übergeben

7. Wie gehen Sie vor, um das Pfeilbild anzuzeigen?
	- erst wird das Asset rausgesucht und dann werden die Koordinaten des Feldes genommen über welches
	  die Maus zurzeit ist. Nun werden die Koordinaten leicht verändert das der Pfeil über dem Tile sitzt.
	  
	  
Fragen zu 4.3:

1. Wie können Sie die Stellenanzahl der übergebenen Zahl bestimmen?
	- die Stellenanzahl kann mit abfragen wie, if(number>99) bestimmt werden. 

2. In welcher Abfolge lassen Sie die Ziffernbilder anzeigen?
	- es wird zuerst die kleinste Stelle der Zahl genommen und angezeigt und dann alle weitere Zahlen

3. Warum sehen Sie ev. die Ziffernbilder nicht, obwohl diese korrekt platziert wurden?

	- die Zifferbilder liegen unter dem Gemalten Rechteck.

4. Wie lassen Sie die Zahlen mittig auf der Kachel anzeigen?
	- mit den Richtigen Koordinaten
	