package de.uniluebeck.itm.schiffeversenken.game.model;

/**
 * This class represents a ship.
 * @author leondietrich, modified by I. Schumacher, modified by C.Horn and J.Schwarz
 *
 */
public class Ship {

	private int length;
	private int hits;
	private boolean orientation;

	/**
	 * This constructor constructs a new ship.
	 * @param length The length of the ship to create.
	 * @param orientation The orientation of the ship: true for vertical, false for horizontal
	 */
	public Ship(int length, boolean orientation) {
		length = 0;
		hits= 0;
		orientation = true;
	}

	/**
	 * Count the hits
	 */
	void hit() {
		if (isSunken() == false) {
			hits = hits +1;
		}		
	}

	/**
	 * Checks if a ship is sunken or not
	 * @return true or false if ship is sunken or not
	 */
	public boolean isSunken() {
		if (hits == length) {
			return true;
		}
		return false;  
	}

	/**
	 * get()-Method for the orientation of the ship
	 * @return orientation of the ship
	 */
	public boolean isUp() {
		if(orientation == true) {
			return true;
		}
		return false;
	}

}
