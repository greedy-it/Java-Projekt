package de.uniluebeck.itm.schiffeversenken.game;

import de.uniluebeck.itm.schiffeversenken.engine.*;
import de.uniluebeck.itm.schiffeversenken.game.model.*;

/**
 * Game field rendering methods.
 *
 * It only contains methods designated to rendering a game field. In future it
 * will also provide scroll bars in case the field doesn't fit on the canvas.
 * 
 * modified by C.Horn and J.Schwarz
 * 
 */
public class GameFieldRenderer {

	private final GameField field;

	public GameFieldRenderer(GameField field) {
		this.field = field;
	}

	/**
	 * Use this method in order to render a game field.
	 *
	 * @param c The canvas to render on.
	 * @param x The x coordinate where to render the field.
	 * @param y The y coordinate where to render the field.
	 * 
	 * modified by C.Horn and J.Schwarz
	 * 
	 */
	public void renderGameField(Canvas c, int x, int y) {
		final int tileSize = Constants.TILE_SIZE;

		// Look up the default water tiles
		final Tile waterTile = AssetRegistry.getTile("water");
		final Tile waterMissed = AssetRegistry.getTile("water.hit");

		// draw the fields tiles
		for (int tileX = 0; tileX < this.field.getSize().getX(); tileX++) {
			for (int tileY = 0; tileY < this.field.getSize().getY(); tileY++) {
				final Vec2 tilePosition = new Vec2(x + tileX * tileSize, y + tileY * tileSize);
				getTileAt(tileX, tileY, waterTile, waterMissed).renderAt(c, tilePosition);
			}
		}

		final int width = tileSize * this.field.getSize().getX();
		final int height = tileSize * this.field.getSize().getY();

		// TODO: draw lines as border between cell

		// draw black lines as border between cell
		// black
		c.setColor(0, 0, 0);
		// draws the vertical black lines
		for (int dx = x; dx < width * 2 - tileSize; dx += tileSize) {
			c.drawLine(new Vec2(dx + tileSize, y), new Vec2(dx + tileSize, height + y));
		}
		// draws the horizontal black lines
		for (int dy = y; dy < height; dy += tileSize) {
			c.drawLine(new Vec2(x, dy + tileSize), new Vec2(width + x, dy + tileSize));
		}

		// TODO: Draw a border around the field

		// Draw a white border around the field
		// white
		c.setColor(1, 1, 1);
		// draws the white border around the field
		c.drawLine(new Vec2(x, y), new Vec2(x + width, y));
		c.drawLine(new Vec2(x + width, y), new Vec2((x + width), y + height));
		c.drawLine(new Vec2(x + width, y + height), new Vec2(x, y + height));
		c.drawLine(new Vec2(x, y + height), new Vec2(x, y));

	}

	/**
	 * This method gets called by the rendering method in order to look up the
	 * correct tile at a given position.
	 * 
	 * @param x            The x coordinate of the tile to look up
	 * @param y            The y coordinate of the tile to look up
	 * @param waterTile    A cached version for a water tile (will be the most
	 *                     returned one)
	 * @param waterHitTile A cached version of the water_missed tile (Will be the
	 *                     second most returned one)
	 * @return The correct tile to render
	 */
	protected Tile getTileAt(int x, int y, Tile waterTile, Tile waterHitTile) {
		switch (this.field.getTileAt(x, y).getTilestate()) {
		default:
			return AssetRegistry.getTile("unknown");
		case STATE_WATER:
			return waterTile;
		case STATE_MISSED:
			return waterHitTile;
		case STATE_SHIP:
			return lookupShipTile(x, y, false);
		case STATE_SHIP_HIT:
			return lookupShipTile(x, y, true);
		}
	}

	/**
	 * This method looks up the correct tile if the position happens to be a ship.
	 * 
	 * @param x          The x coordinate of the ships tile
	 * @param y          The y coordinate of the ships tile
	 * @param alreadyHit A Flag that indicates whether nor not the tile has been hit
	 *                   yet.
	 * @return The composed ships tile
	 * 
	 * modified by C. Horn and J. Schwarz
	 * 
	 */
	private Tile lookupShipTile(int x, int y, boolean alreadyHit) {
		// local variable ship with x-/y-coordinates from getTileAt (GameField class) to the corresponding Ship
		Ship ship = this.field.getTileAt(x, y).getCorrespondingShip();
		// 4 boolean to watch around a tile
		boolean over  = false;
		boolean under = false;
		boolean left  = false;
		boolean right = false;
		
		//watches the game field boundaries for the left hand side
		if (x < field.getSize().getX() - 1) {
			// gives true if the tile is corresponding to the ship else false
			right = (ship == this.field.getTileAt(x + 1, y).getCorrespondingShip());
		}
		//watches the game field boundaries for the right hand side
		if (x > 0) {
			// gives true if the tile is corresponding to the ship else false
			left  = (ship == this.field.getTileAt(x - 1, y).getCorrespondingShip());
		}
		//watches the game field boundaries for the above side
		if (y < field.getSize().getY() - 1) {
			// gives true if the tile is corresponding to the ship else false
			under = (ship == this.field.getTileAt(x, y + 1).getCorrespondingShip());
		}
		//watches the game field boundaries for the below side
		if (y > 0) {
			// gives true if the tile is corresponding to the ship else false
			over  = (ship == this.field.getTileAt(x, y - 1).getCorrespondingShip());
		}
		
		// for the vertical ship placement
		if (ship.isUp() == true) {
			// watches tiles over, under, left and right from the ship tile
			if (over == true && under == true) {
				// return the hitting tile if true else the normal ship tile
				// ?-operator watches alreadyHit and gives the return statement for true or false
				return (alreadyHit == true) ? AssetRegistry.getTile("up.ship.middle.hit") : AssetRegistry.getTile("up.ship.middle");
			}
			if (over == false && under == true) {
				return (alreadyHit == true) ? AssetRegistry.getTile("up.ship.bug.hit") : AssetRegistry.getTile("up.ship.bug");
			}
			if (over == true && under == false) {
				return (alreadyHit == true) ? AssetRegistry.getTile("up.ship.aft.hit") : AssetRegistry.getTile("up.ship.aft");
			}
			return (alreadyHit == true) ? AssetRegistry.getTile("up.ship.single.hit") : AssetRegistry.getTile("up.ship.single");
		}
		
		// for the horizontal ship placement
		if (ship.isUp() == false) {
			if (right == true && left == true) {
				return (alreadyHit == true) ? AssetRegistry.getTile("right.ship.middle.hit") : AssetRegistry.getTile("right.ship.middle");
			}
			if (right == false && left == true) {
				return (alreadyHit == true) ? AssetRegistry.getTile("right.ship.bug.hit") : AssetRegistry.getTile("right.ship.bug");
			}
			if (right == true && left == false) {
				return (alreadyHit == true) ? AssetRegistry.getTile("right.ship.aft.hit") : AssetRegistry.getTile("right.ship.aft");
			}
		}
		return (alreadyHit == true) ? AssetRegistry.getTile("right.ship.single.hit") : AssetRegistry.getTile("right.ship.single");
	}

	/**
	 * The purpose of this method is to enable the usage of the game field on
	 * expanding classes.
	 * 
	 * @return The game field
	 */
	protected GameField getField() {
		return this.field;
	}

}
