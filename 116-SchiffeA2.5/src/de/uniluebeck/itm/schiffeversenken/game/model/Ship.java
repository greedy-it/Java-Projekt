package de.uniluebeck.itm.schiffeversenken.game.model;

/**
 * This class represents a ship.
 * @author leondietrich, modified by I. Schumacher, 
 * modified by C. Horn and J. Schwarz
 *
 */
public class Ship {

	private int length; 			 // length of the ship
	private int hits;				// counter for hits
	private boolean orientation;	 // orientation of the ship (vertical or horizontal)

	/**
	 * This constructor constructs a new ship.
	 * @param length The length of the ship to create.
	 * @param orientation The orientation of the ship: true for vertical, false for horizontal
	 * 
	 * modified by C. Horn and J. Schwarz
	 * 
	 */
	public Ship(int length, boolean orientation) {
		this.length = length;
		// counter for hits
		hits = 0;
		this.orientation = orientation;
	}

	/**
	 * Count the hits
	 */
	void hit() {
		if (!isSunken()) {
			hits += hits;
		}		
	}

	/**
	 * Checks if a ship is sunken or not
	 * @return true or false if ship is sunken or not
	 */
	public boolean isSunken() {
		return this.hits == this.length;  
	}

	/**
	 * get()-Method for the orientation of the ship
	 * @return orientation of the ship
	 */
	public boolean isUp() {
		return this.orientation;
	}

}
