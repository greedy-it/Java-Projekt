package de.uniluebeck.itm.schiffeversenken.game.model;

/**
 * This class represents a ship.
 * @author leondietrich, modified by I. Schumacher
 *
 */
public class Ship {
	
	
	/**
	 * This constructor constructs a new ship.
	 * @param lenght The length of the ship to create.
	 * @param orientation The orientation of the ship: true for vertical, false for horizontal
	 */
	public Ship(int length, boolean orientation) {
		
	}
	
	/**
	 *
	 */
	void hit() {
		
	}

	/**
	 * 
	 * 
	 */
	public boolean isSunken() {
		return false;  
	}

}
