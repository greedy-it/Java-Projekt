package de.uniluebeck.itm.schiffeversenken.game;

import de.uniluebeck.itm.schiffeversenken.engine.*;
import de.uniluebeck.itm.schiffeversenken.game.model.GameField;

/**
 * Hide ships in the opponent's playfield
 * w/o the game also displays the opponent's ships
 * 
 * Created by C.Horn and J.Schwarz
 * 
 */

public class HitMissRenderer extends GameFieldRenderer{
		
	public HitMissRenderer(GameField field) {
		super(field);
	}

	@Override
	protected Tile getTileAt(int x, int y, Tile waterTile, Tile waterHitTile) {
		// access the matrix via the get() method of the superclass, since the variable is declared private there.
		switch (getField().getTileAt(x, y).getTilestate()) {
		default:
			return waterTile;
		case STATE_MISSED:
			return waterHitTile;
		case STATE_SHIP_HIT:
			return AssetRegistry.getTile("water.hiddenshiphit");
		}			
	}
}
