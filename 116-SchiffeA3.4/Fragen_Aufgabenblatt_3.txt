                                   Fragen zum Aufgabenblatt 03

Fragen zu 3.1:
1. Was muss beim Konstruktor der erbenden Klasse bzgl. der Parameterübergabe beachtet werden?
	- da in Java Konstruktoren nicht vererbt werden, kann mit Hilfe des Schlüsselworts super auf
	  die Konstruktoren der Vaterklasse (Superklasse) zugegriffen und dadurch bereits vorhandener 
	  Quellcode wiederverwendet werden

2. Warum kann ich in der erbenden Klasse nicht direkt mit der Variablen field arbeiten?
	- weil "field" aus GameFieldRenderer kommt, also aus der Superklasse und deshalb mit Hilfe von super(field)
	  daraufzugegriffen wird

3. Was bedeutet das Überschreiben einer Methode?
	- das eine bereits integrierte Methode einer Klasse, von der gleichnamigen Methode einer vererbten Klasse
	  benutzt wird und die Standardmethode, überschrieben wird, aus der Methode die aus der Vaterklasse kommt


Fragen zu 3.2:
1. Was berechnet die Anweisung 
      Vec2 positionOnOpponentsField = mousePosition.add(model.getOpponentsFieldPosition().multiply(-1));
    - es nimmt die Mausposition auf dem Spielfeld, also die x und y Koordinanten und multipliziert es per Skalarmultiplikation
      mit -1, um die gleichen Koordinaten wie auf dem menschlichen Spielfeld zu bekommen
      	
2. Skizze des Feldes mit positionOnOpponentsField und res
	- ergibt die 16x16 Matrix, damit es egal ist, wo wir in dem Feld klicken, der Treffer immer in dem Feld ist


3. Wie werden aus positionOnOpponentsField die Feldkoordinaten berechnet?
	- es werden die Mauskoordinanten genommen und durch die TileSize(32) geteilt. Daraus ergeben sich die genauen Felder


4. Warum ist die Abfrage  
       if (!fieldTile.wasAlreadyBombarded()) { ... 
   notwendig? Gibt es in der Klasse FieldTile ein Attribut alreadyBombarded oder was macht die Methode?
	- es prüft, ob die Stelle schon bombardiert wurde oder eben nicht
	- es gibt in der FieldTile.java diese Methode, die den TILE_STATE prüft
  
   
5. Woraus ersieht man, dass die Spieler nach einem Treffer erneut drankommen, also kein Wechsel erfolgt?
	- setRoundChangingFlag() ist false
	


6. In welcher Klasse befindet sich die Methode, die die Kopie der Schiffsliste liefert? Wie können Sie 
   aus der Klasse GameController darauf zugreifen?
	- die Methode liegt in GameField.java
	- zugreifen kann man mit Hilfe von this.getModelInstance().getComputerPlayerField().getCopyOfShipListAsArray();
   
   
7. Warum macht es Sinn, bei 2dii) eine Hilfsmethode anzulegen?
	- weil man mit Hilfe der Methode einfacher es programmieren kann, wer das Spiel gewonnen hat